# Hashes
Dette programmet tar en mappe og hasher alle filene som er i den, og alle
undermapper.

## Valg
Brukeren kan velge mellom tre valg:
  * Hash alle filer fra en gitt mappe-rot til en indre mappe.
  * Verifiser alle filer -||-
  * Verifiser en enkel fil.
  * Bytt mappe-rot.