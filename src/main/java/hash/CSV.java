package hash;

/*
Created by: William Slettvold Østensen
Date: 15-11-2019
*/

class CSV {
  private String[] csv;

  public CSV(String str) {
    csv = str.split(",");
  }

  public String first() {
    return csv[0];
  }

  public String second() {
    return csv[1];
  }

}