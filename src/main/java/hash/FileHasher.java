/*
Created by: William Slettvold Østensen
Date: 13-11-2019
*/
package hash;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.codec.digest.MessageDigestAlgorithms;

class FileHasher {

  public static final String RESET  = "\u001B[0m";
  public static final String BLACK  = "\u001B[30m";
  public static final String RED    = "\u001B[31m";
  public static final String GREEN  = "\u001B[32m";
  public static final String YELLOW = "\u001B[33m";
  public static final String BLUE   = "\u001B[34m";
  public static final String PURPLE = "\u001B[35m";
  public static final String CYAN   = "\u001B[36m";
  public static final String WHITE  = "\u001B[37m";

  public static String home = System.getProperty("user.home");
  public static String path = home;
  public static String entryName;

  public static File writeFilePath;

  public static final Scanner scanner = new Scanner(System.in);

  public static void main(String[] args) {
    
    System.out.println(color("=== WELCOME ===", GREEN));
    printCommands();
    
    String command = scanner.nextLine();

    while(command.charAt(0) != 'q') {
      switch(command) {
        case "1":
          hashAll();
          break;
        case "2":
          changeDirectory();
          break;
        case "3":
          verifyDirectory();
          break;
        case "4":
          verifyFile();
          break;
        default:
          System.err.println("Unknown command.");
          break;
      }
      printCommands();
      command = scanner.nextLine();
    }
    scanner.close();
    System.out.println("Goodbye.");
  }

  public static void changeDirectory() {
    // Prompts the user for a new directory.
    System.out.print(color("\nNew path: ", GREEN));
    String newPath = scanner.nextLine();
    // Adds the "new directory" to the old, creating a new.
    File newFilePath = new File(path + "\\" + newPath);

    // Check if the directory actually exists.
    if(newFilePath.isDirectory()) 
      path = newFilePath.getAbsolutePath();
    else
      System.err.println("Directory does not exist!");
  }

  public static void hashAll() {
    System.out.println(color("Trying to read: ", GREEN) 
    + path + "... ");
    writeFilePath = new File(String.format("%s\\hashes", path));
    try {
      if(writeFilePath.createNewFile()) {
        System.out.println(
          color("Could not find hash file, created new hash file.", RED)
        );
      }
    } catch(IOException e) {
      System.err.println(e.getMessage());
    }

    clearFile(writeFilePath);

    System.out.println();
    search(new File(path));
  }

  public static void verifyDirectory() {
    if(!hasHash()) return;
    try {
      int verified = 0, changed = 0;
      List<String> hashes = new ArrayList<>();
      HashMap<String, Boolean> result = new HashMap<>(); 
      hashes = Files.readAllLines(new File(path + "\\hashes").toPath());
      for(String line : hashes) {
        CSV csv;
        csv = new CSV(line);
        String fileContent = hashFileContent(new File(csv.first()));
        result.put(csv.first(), fileContent.equals(csv.second()));
      }
      for(String key : result.keySet()) 
        if(result.get(key)) {
          verified++;
        } else {
          changed++;
          System.out.println(
            color(String.format("File %s is changed!", key), RED)
          );
        }
        System.out.println(
          color(
            String.format(
              "Scan complete. %d is not changed, %d is changed.", 
              verified, 
              changed),
              GREEN)
        );
      }
      catch(IOException e) {
        System.out.println(e.getMessage());
      }
    }

  public static void verifyFile() {
    // En veldig stygg funksjon, hadde jeg fått dette som en oppgave
    // hadde jeg refaktorisert funksjonen ytterligere.
    if(!hasHash()) return;
    ArrayList<String> hashFile;
    String fileToVerify = scanner.nextLine();
    String fullPath;
    File verify, hashFile2 = new File(path + "\\hashes");

    System.out.print(color("\nFile to verify (-q to quit): ", GREEN));
    if(fileToVerify.equals("-q")) return;
    verify = new File(path + "\\" + fileToVerify);

    while(verify == null && !fileToVerify.equals("-q")) {
      fileToVerify = scanner.nextLine();
      verify = new File(path + "\\" + fileToVerify);
    }

    try {
      fullPath = verify.getAbsolutePath();
      hashFile 
      = (ArrayList<String>) Files.readAllLines(hashFile2.toPath());

      for(String line : hashFile) {
        CSV csv;
        csv = new CSV(line);
        if(csv.first().equals(fullPath)) {
          String fileContent = hashFileContent(new File(fullPath));
          if(csv.second().equals(fileContent)) {
            System.out.println(color("The file is verified.", GREEN));
            return;
          } else {
            System.out.println(color("File is changed!", RED));
            return;
          }
        }
      }

      System.err.println("Could not find file: " + fullPath);

    } catch(IOException e) {
      System.err.println(e.getMessage());
    }

  }

  public static void search(File file) {
    File entry;
    String fileContentHash;
    String directory[] = file.list();

    if(directory == null) return;
    // Checks if there is just one file in the directory, and if that file
    // is itself (empty folder with only a folder), compress the name.
    if(directory.length == 1
        && file.isDirectory()) {
          System.out.print(BLUE + file.getName() + "/"); 
    } else {
      System.out.print(BLUE + file.getName() + RESET + "\n");
    }

    for (int i = 0; i < directory.length; i++) {
        entry = new File(file, directory[i]);
        // Ignore all '.', '..' and 'hidden files'.
        if(directory[i].charAt(0) == '.') continue;
        if(entry.isDirectory()) search(entry);
        if(entry.isFile()) {
          if(entry.getName().equals("hashes")) continue;
          System.out.println(entry.getName());
          fileContentHash = new String(hashFileContent(entry));
          if(fileContentHash != null) {
            // Prints in a CSV compliant format.
            writePathToFile(writeFilePath, 
              entry.getAbsolutePath() + "," + fileContentHash.toString());
          }
        }
    }
  }

  public static String hashFileContent(File file) {
    try {
      // Digests the files content, and spits out a String
      // containing the hex value of the hash.
      return new DigestUtils(MessageDigestAlgorithms.SHA_256)
                .digestAsHex(Files.readAllBytes(file.toPath()));
    } catch(Exception e) {
      System.err.println(e.getMessage());
    }
    return null;
  }

  public static void clearFile(File file) {
    // Just clear the file; used before hashing all the files again.
    try {
      Files.write(file.toPath(), "".getBytes());
    } catch(IOException e) {
      System.err.println(e.getMessage());
    }
  }

  public static void writePathToFile(File file, String text) {
    // Helping function to write to a file.
    try {
      Files.write(file.toPath(), 
                  (text + "\n").getBytes(), StandardOpenOption.APPEND);
    } catch(IOException e) {
      System.err.println(e.getMessage());
    }
  }

  public static String color(String str, String color) {
    // Helping function to make the output prettier.
    return color + str + RESET;
  }

  public static void printCommands() {
    // Just to stay inline with DRY. I don't want to have to replace
    // the text all the places when I want to do a simple change.
    System.out.print(
      color("Welcome, current directory is: ", GREEN) +
      path + "\n" +
      color("(1) Hash all files from here to inner-most folder.\n", BLUE) +
      color("(2) Change directory.\n", BLUE) +
      color("(3) Verify directory.\n", BLUE) +
      color("(4) Verify file.\n", BLUE) + 
      color("(q/quit) Quit program.\n", BLUE) +
      "Enter command: "
    );
  }

  public static boolean hasHash() {
    return Files.exists(new File(path + "\\hashes").toPath());
  }
}